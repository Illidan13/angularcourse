import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurPricesComponent } from './our-prices/our-prices.component';
import {OurPricesRoutingModule} from "./our-prices-routing.module";
import {TranslocoModule} from "@ngneat/transloco";



@NgModule({
    declarations: [
        OurPricesComponent
    ],
    exports: [
        OurPricesComponent
    ],
    imports: [
        CommonModule,
        OurPricesRoutingModule,
        TranslocoModule
    ]
})
export class OurPricesModule { }
