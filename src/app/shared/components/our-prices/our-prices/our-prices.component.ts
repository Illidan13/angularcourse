import { Component } from '@angular/core';
import {SUBSCRIBTION_PRICING} from "../../../static/home.static";
import {Pricing} from "../../../models/models";

@Component({
  selector: 'app-our-prices',
  templateUrl: './our-prices.component.html',
  styleUrls: ['./our-prices.component.scss']
})
export class OurPricesComponent {
  subscribeCards: Pricing[] = SUBSCRIBTION_PRICING;

  isDoubleCard(num: number): boolean {
    return Number(num)%2 !== 0;
  }
}
