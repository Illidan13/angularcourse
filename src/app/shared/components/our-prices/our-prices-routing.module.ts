import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OurPricesComponent} from "./our-prices/our-prices.component";
const routes: Routes = [
  {
    path: '', component: OurPricesComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OurPricesRoutingModule {

}
