import { Component } from '@angular/core';
import {HEADER_LINKS, LANGUAGES, link} from "../../static/header.static";
import {TranslocoService} from "@ngneat/transloco";
import {Clipboard} from "@angular/cdk/clipboard";
import {LocalStorageService} from "../../services/local-storage.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  links: link[] = HEADER_LINKS;
  dropdownVisibility: boolean = false;
  selectedLanguage: string;
  languages: string[] = LANGUAGES;
  showBurger: boolean = false
  isCopied: boolean = false;

  constructor(private translocoService: TranslocoService, private clipboard: Clipboard, private ls: LocalStorageService) {
    this.selectedLanguage = this.ls.getLanguage();
  }

  showDropdown() {
    this.dropdownVisibility = !this.dropdownVisibility;
  }

  setLanguage(language: string) {
    this.selectedLanguage = language
    this.translocoService.setActiveLang(language);
    this.ls.setLanguage(language)
  }

  flipBurger() {
    this.showBurger = !this.showBurger;
  }
}
