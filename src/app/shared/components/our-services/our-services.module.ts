import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurServicesComponent } from './our-services/our-services.component';
import {OurServicesRoutingModule} from "./our-services-routing.module";
import {TranslocoModule} from "@ngneat/transloco";



@NgModule({
  declarations: [
    OurServicesComponent,
  ],
  exports: [
    OurServicesComponent
  ],
  imports: [
    CommonModule,
    OurServicesRoutingModule,
    TranslocoModule
  ]
})
export class OurServicesModule { }
