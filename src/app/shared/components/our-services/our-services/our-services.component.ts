import { Component } from '@angular/core';
import { SERVICES_CARDS } from "../../../static/home.static";
import {Card} from "../../../models/models";

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.scss']
})
export class OurServicesComponent {
  Cards: Card[] = SERVICES_CARDS;
}
