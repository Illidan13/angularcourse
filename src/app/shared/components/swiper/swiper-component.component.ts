import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Review} from "../../models/models";

@Component({
  selector: 'app-swiper-component',
  templateUrl: './swiper-component.component.html',
  styleUrls: ['./swiper-component.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SwiperComponentComponent {
  @Input() reviews!: Review[];

  swiperConfig = {
    pagination: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      992: {
        slidesPerView: 2
      }
    }
  };
}
