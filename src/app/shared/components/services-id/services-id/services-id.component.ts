import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { INTEREER_CARDS, SERVICES_CARDS} from "../../../static/home.static";
import {Subscription} from "rxjs";
import {Card, InterCard} from "../../../models/models";

@Component({
  selector: 'app-services-id',
  templateUrl: './services-id.component.html',
  styleUrls: ['./services-id.component.scss'],
})
export class ServicesIdComponent implements OnInit, OnDestroy{
  currentService?: Card;
  designs:InterCard[] = INTEREER_CARDS;
  sSub!: Subscription;

  constructor(private route: ActivatedRoute, private router: Router) {
  }


  ngOnInit() {
     this.getParam()
  }

  ngOnDestroy() {
    this.sSub.unsubscribe();
  }

  getParam() {
    this.sSub = this.route.params.pipe().subscribe(param => {
      const currentId = param['id'];
      this.currentService = SERVICES_CARDS.find(card => card.id === currentId)

      if(!this.currentService) {
        this.router.navigate(['**']);
      }
    })
  }

}
