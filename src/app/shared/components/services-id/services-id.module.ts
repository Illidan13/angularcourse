import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ServicesIdComponent} from "./services-id/services-id.component";
import {ServicesIdRoutingModule} from "./services-id-routing.module";
import {TranslocoModule} from "@ngneat/transloco";
import {SwiperModule} from "swiper/angular";



@NgModule({
  declarations: [
    ServicesIdComponent
  ],
    imports: [
        CommonModule,
        ServicesIdRoutingModule,
        TranslocoModule,
        SwiperModule
    ]
})
export class ServicesIdModule { }
