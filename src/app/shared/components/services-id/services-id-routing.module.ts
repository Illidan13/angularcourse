import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ServicesIdComponent} from "./services-id/services-id.component";

const routes: Routes = [
  {
    path: '', component: ServicesIdComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServicesIdRoutingModule {

}
