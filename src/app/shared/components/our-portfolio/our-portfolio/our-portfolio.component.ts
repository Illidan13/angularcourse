import {Component, Input, OnInit} from '@angular/core';
import {
  Filters,
  INTEREER_CARDS,
  PORTFOLIO_CARDS_ON_MAIN_PAGE,
  PORTFOLIO_CARDS_ON_PORTFOLIO_PAGE
} from "../../../static/home.static";
import {LocationStrategy} from "@angular/common";
import {Filter, InterCard} from "../../../models/models";

@Component({
  selector: 'app-our-portfolio',
  templateUrl: './our-portfolio.component.html',
  styleUrls: ['./our-portfolio.component.scss']
})
export class OurPortfolioComponent implements OnInit{
  @Input() cardsOnPage: number = PORTFOLIO_CARDS_ON_PORTFOLIO_PAGE;
  filters: Filter[] = Filters;
  intereerCards: InterCard[] = INTEREER_CARDS;
  allCardsLoaded: boolean = false

  activeFilter: string = '';
  showedCards: number = PORTFOLIO_CARDS_ON_MAIN_PAGE;

  constructor( private readonly url: LocationStrategy) {
  }

  ngOnInit() {
    this.setActive('');
  }

  setActive(tabIndex: string) {
    this.activeFilter = tabIndex;
    this.intereerCards = [];
    INTEREER_CARDS.forEach((card) => {
      if(this.checkForFilter(card) && !this.checkForOutLimit()) {
        this.intereerCards.push(card);
      }
    })
  }

  isPortfolioPage(): boolean {
    const route = this.url.path();
    return route.includes('/portfolio');
  }

  addMoreCards() {
    this.cardsOnPage += 12;
    this.setActive(this.activeFilter)
    this.allCardsLoaded = this.cardsOnPage > this.intereerCards.length;
  }

  private checkForOutLimit():boolean {
    return !! this.cardsOnPage && this.intereerCards.length >= this.cardsOnPage;
  }

  private checkForFilter(card: InterCard):boolean {
    return card.category_index === this.activeFilter || this.activeFilter === '';
  }

}
