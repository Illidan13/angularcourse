import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OurPortfolioComponent} from "./our-portfolio/our-portfolio.component";

const routes: Routes = [
  {
    path: '', component: OurPortfolioComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OurPortfolioRoutingModule {

}
