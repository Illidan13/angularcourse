import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurPortfolioComponent } from './our-portfolio/our-portfolio.component';
import {TranslocoModule} from "@ngneat/transloco";
import {RouterLink} from "@angular/router";
import {OurPortfolioRoutingModule} from "./our-portfolio-routing.module";



@NgModule({
  declarations: [
    OurPortfolioComponent
  ],
  exports: [
    OurPortfolioComponent
  ],
  imports: [
    CommonModule,
    OurPortfolioRoutingModule,
    TranslocoModule,
    RouterLink
  ]
})
export class OurPortfolioModule { }
