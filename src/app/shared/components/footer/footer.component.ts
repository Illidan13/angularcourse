import { Component } from '@angular/core';
import {link} from "../../static/header.static";
import { FOOTER_LINKS} from "../../static/footer.static";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  Navigations: link[] = FOOTER_LINKS;
}
