import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FooterComponent} from "./components/footer/footer.component";
import {HeaderComponent} from "./components/header/header.component";
import {SwiperComponentComponent} from "./components/swiper/swiper-component.component";
import {SwiperModule} from "swiper/angular";
import {TranslocoModule} from "@ngneat/transloco";
import {RouterLink} from "@angular/router";
import {OurPortfolioModule} from "./components/our-portfolio/our-portfolio.module";
import {OurPricesModule} from "./components/our-prices/our-prices.module";
import {OurServicesModule} from "./components/our-services/our-services.module";
import {ServicesIdModule} from "./components/services-id/services-id.module";
import { TimerDirective } from './directive/timer/timer.directive';



@NgModule({
  declarations: [FooterComponent, HeaderComponent, SwiperComponentComponent, TimerDirective],
  imports: [
    CommonModule,
    SwiperModule,
    TranslocoModule,
    RouterLink,
    OurPortfolioModule,
    OurPricesModule,
    OurServicesModule,
    ServicesIdModule,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    SwiperComponentComponent,
    TimerDirective,
  ]
})
export class SharedModule { }
