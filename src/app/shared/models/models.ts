export interface Card {
  id: string
  image: string;
  title: string;
  text: string;
}

export interface Filter {
  text: string;
  index: string
  isActive: boolean;
}

export interface InterCard {
  id?: string;
  text: string;
  image: string;
  category_index: string;
}
export interface Pricing {
  id: string;
  amount: number;
  name: string;
  description: string[];
  text: string;
  image: string;
}
export interface Review {
  id: string;
  name: string;
  avatar: string;
  duty: string;
  text: string;
  tweeter_url?: string;
  linkedIn_url?: string;
  facebook_url?: string;
}
