import {link} from "./header.static";

export const FOOTER_LINKS: link[] = [
  {
    text: "About",
    url: "about"
  },
  {
    text: "Services",
    url: "services"
  },
  {
    text: "Portfolio",
    url: "portfolio"
  },
  {
    text: "Pricing",
    url: "pricing"
  },
  {
    text: "Testemonials",
    url: "testemonials"
  },
]

export interface contact {
  title: string;
  text: string;
}


