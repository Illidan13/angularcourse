import {Card, Filter, InterCard, Pricing, Review} from "../models/models";

export const SERVICES_CARDS: Card[] = [
  {
    id: '1',
    image: "assets/images/icon1.png",
    title: "interiorDesign",
    text: "interiorDesignText",
  },
  {
    id: '2',
    image: "assets/images/icon2.png",
    title: "decorativeServices",
    text: "decorativeServicesText",

  },
  {
    id: '3',
    image: "assets/images/icon3.png",
    title: "spacePlanning",
    text: "spacePlanningText"
  },
  {
    id: '4',
    image: "assets/images/icon4.png",
    title: "projectManagement",
    text: "projectManagementText"
  },
]

export const Filters: Filter[] = [
  {
    text: 'All',
    index: '',
    isActive: true
  },
  {
    text: 'Commercial',
    index: 'commercial',
    isActive: false
  },
  {
    text: 'Residential',
    index: 'resident',
    isActive: false
  },
  {
    text: 'Office',
    index: 'office',
    isActive: false
  },
  {
    text: 'Other',
    index: 'other',
    isActive: false
  }
]

export const INTEREER_CARDS: InterCard[] = [
  {
    id: '1',
    text: 'TEXT',
    image: 'assets/images/InterCards/img1.png',
    category_index: 'commercial'
  },
  {
    id: '2',
    text: 'TEXT',
    image: 'assets/images/InterCards/img2.png',
    category_index: 'office'
  },
  {
    id: '3',
    text: 'TEXT',
    image: 'assets/images/InterCards/img3.png',
    category_index: 'commercial'
  },
  {
    id: '4',
    text: 'TEXT',
    image: 'assets/images/InterCards/img4.png',
    category_index: 'resident'
  },
  {
    id: '5',
    text: 'TEXT',
    image: 'assets/images/InterCards/img5.png',
    category_index: 'other'
  },
  {
    id: '6',
    text: 'TEXT',
    image: 'assets/images/InterCards/img6.png',
    category_index: 'commercial'
  },
  {
    id: '7',
    text: 'TEXT',
    image: 'assets/images/InterCards/img7.png',
    category_index: 'commercial'
  },
  {
    id: '8',
    text: 'TEXT',
    image: 'assets/images/InterCards/img8.png',
    category_index: 'office'
  },
  {
    id: '9',
    text: 'TEXT',
    image: 'assets/images/InterCards/img1.png',
    category_index: 'commercial'
  },
  {
    id: '10',
    text: 'TEXT',
    image: 'assets/images/InterCards/img2.png',
    category_index: 'office'
  },
  {
    id: '11',
    text: 'TEXT',
    image: 'assets/images/InterCards/img3.png',
    category_index: 'commercial'
  },
  {
    id: '12',
    text: 'TEXT',
    image: 'assets/images/InterCards/img4.png',
    category_index: 'resident'
  },
  {
    id: '13',
    text: 'TEXT',
    image: 'assets/images/InterCards/img5.png',
    category_index: 'other'
  },
  {
    id: '14',
    text: 'TEXT',
    image: 'assets/images/InterCards/img6.png',
    category_index: 'commercial'
  },
  {
    id: '15',
    text: 'TEXT',
    image: 'assets/images/InterCards/img7.png',
    category_index: 'commercial'
  },
  {
    id: '16',
    text: 'TEXT',
    image: 'assets/images/InterCards/img8.png',
    category_index: 'office'
  },
]

export const SUBSCRIBTION_PRICING: Pricing[] = [
  {
    id: "1",
    amount: 25,
    name: "Basic",
    description: [
      'Interior Design',
      'Project Discussion',
      'Space Planning',
      'Online Consultation'
    ],
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cumque dolor, laudantium maxime odit quos sint ut. Aut cupiditate explicabo ipsum iste iure laboriosam libero, modi obcaecati perspiciatis quae tempora.',
    image: 'assets/images/InterCards/img2.png'
  },
  {
    id: "2",
    amount: 50,
    name: "Standart",
    description: [
      'Color Analysis',
      'Space Planning',
      'Home Remodeling',
      '3D Interior Model'
    ],
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cumque dolor, laudantium maxime odit quos sint ut. Aut cupiditate explicabo ipsum iste iure laboriosam libero, modi obcaecati perspiciatis quae tempora.',
    image: 'assets/images/InterCards/img5.png'
  },
  {
    id: "3",
    amount: 80,
    name: "Premium",
    description: [
      'Concept Development',
      'Decoration Services',
      'Interior Architecture',
      'Flooring Installation'
    ],
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cumque dolor, laudantium maxime odit quos sint ut. Aut cupiditate explicabo ipsum iste iure laboriosam libero, modi obcaecati perspiciatis quae tempora.',
    image: 'assets/images/InterCards/img8.png'
  }
]

export const REWIEWS: Review[] = [
  {
    id: "1",
    name: "Annette Black",
    avatar: "assets/images/avatars/billy.png",
    duty: "Merchandising Associate",
    text: "Convallis non proin ac bibendum. At pharetra sed ultrices semper. Lectus et ornare ultrices urna in. Id non fringilla dignissim ac risus amet eget egestas vestibulum. Aliquet etiam eget nulla nulla odio vel.",
    tweeter_url: "tweeter.com",
    linkedIn_url: "linkedin.com",
    facebook_url: "facebook.com"
  },
  {
    id: "2",
    name: "Marvin McKinney",
    avatar: "assets/images/avatars/van.png",
    duty: "Administrator",
    text: "Leo viverra vestibulum amet, scelerisque. Vitae, ultrices mi rhoncus porttitor nulla sed sodales. Curabitur tellus massa, lacinia non at. Lacus, hac fermentum, viverra tortor, eget. Eget egestas eget ultrices vitae.",
    tweeter_url: "tweeter.com",
    facebook_url: "facebook.com"
  },
  {
    id: "3",
    name: "Marvin McKinney",
    avatar: "assets/images/avatars/van.png",
    duty: "Administrator",
    text: "Leo viverra vestibulum amet, scelerisque. Vitae, ultrices mi rhoncus porttitor nulla sed sodales. Curabitur tellus massa, lacinia non at. Lacus, hac fermentum, viverra tortor, eget. Eget egestas eget ultrices vitae.",
    tweeter_url: "tweeter.com",
    facebook_url: "facebook.com"
  },
]

export const PORTFOLIO_CARDS_ON_MAIN_PAGE = 8;

export const PORTFOLIO_CARDS_ON_PORTFOLIO_PAGE = 12;

