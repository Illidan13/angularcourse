export interface link {
  text: string;
  url: string;
}

export const HEADER_LINKS: link[] = [
  {
    text: 'Services',
    url: 'services'
  },
  {
    text: 'Portfolio',
    url: 'portfolio'
  },
  {
    text: 'Pricing',
    url: 'pricing'
  },
]

export const LANGUAGES: string[] = [
  'en',
  'ua'
]
