import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';

@Directive({
  selector: '[appTimer]'
})
export class TimerDirective implements OnChanges, OnDestroy{
  @Input('appTimer') trigger: boolean = false
  @Output() revertTrigger = new EventEmitter<boolean>()
  timeout: any;

  constructor(private el: ElementRef, private r: Renderer2) {
    this.r.setStyle(this.el.nativeElement, 'display', 'none')
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.trigger) {
      this.showSuccess()
      this.revertTrigger.emit()
    }
  }

  ngOnDestroy() {
  }

  showSuccess() {
    this.r.setStyle(this.el.nativeElement, 'display', 'block')
    this.timeout = setTimeout(() => {
      this.r.setStyle(this.el.nativeElement, 'display', 'none')
    },3000);
    clearTimeout(this.timeout);
  }

}
