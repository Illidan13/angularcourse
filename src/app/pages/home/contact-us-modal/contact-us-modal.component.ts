import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-contact-us-modal',
  templateUrl: './contact-us-modal.component.html',
  styleUrls: ['./contact-us-modal.component.scss']
})
export class ContactUsModalComponent implements OnInit{
  @Output() close = new EventEmitter<void>()
  form!: FormGroup;
  showSuccessMessage = false;

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      message: new FormControl(null, [Validators.required])
    })
  }

  isRequired(field: string): boolean {
    return !! this.form.get(field)!.errors!['required'];
  }

  isFormInvalid(field: string): boolean {
    return this.form.get(field)!.touched && this.form.get(field)!.invalid
  }

  isEmailInvalid(): boolean {
    return this.form!.get('email')!.errors!['email'];
  }

  submit() {
    if(this.form.invalid) {
      return
    }

    console.log(`Your name: ${this.form.value.name}, your email: ${this.form.value.email}, your message: ${this.form.value.message}`)
    this.form.reset()
    this.showSuccessMessage = true;
  }

}
