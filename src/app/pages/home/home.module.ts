import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {TranslocoModule} from "@ngneat/transloco";
import {HomeRoutingModule} from "./home-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {OurServicesModule} from "../../shared/components/our-services/our-services.module";
import {OurPortfolioModule} from "../../shared/components/our-portfolio/our-portfolio.module";
import { ContactUsModalComponent } from './contact-us-modal/contact-us-modal.component';
import {ReactiveFormsModule} from "@angular/forms";
import {OurPricesModule} from "../../shared/components/our-prices/our-prices.module";

@NgModule({
  declarations: [
    HomeComponent,
    ContactUsModalComponent,
  ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        TranslocoModule,
        SharedModule,
        OurServicesModule,
        OurPortfolioModule,
        ReactiveFormsModule,
        OurPricesModule,
    ]
})
export class HomeModule { }
