import {Component} from '@angular/core';
import SwiperCore, {Pagination} from "swiper";

import {
  INTEREER_CARDS, REWIEWS,
  PORTFOLIO_CARDS_ON_MAIN_PAGE
} from "../../../shared/static/home.static";
import {InterCard, Review} from "../../../shared/models/models";

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  intereerCards: InterCard[] = INTEREER_CARDS;
  reviews: Review[] = REWIEWS;

  activeFilter: string = '';
  showAllAbout: boolean = false;
  viewCards: number = PORTFOLIO_CARDS_ON_MAIN_PAGE;
  modal: boolean = false;

  switchAbout() {
    this.showAllAbout = !this.showAllAbout;
  }
}
