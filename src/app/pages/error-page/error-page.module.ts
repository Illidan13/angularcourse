import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorPageComponent } from './error-page/error-page.component';
import {ErrorPageRoutingModule} from "./error-page-routing.module";
import {TranslocoModule} from "@ngneat/transloco";



@NgModule({
  declarations: [
    ErrorPageComponent
  ],
  imports: [
    CommonModule,
    ErrorPageRoutingModule,
    TranslocoModule
  ]
})
export class ErrorPageModule { }
