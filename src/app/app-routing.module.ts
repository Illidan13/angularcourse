import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'services',
    loadChildren: () => import('./shared/components/our-services/our-services.module').then(m => m.OurServicesModule)
  },
  {
    path: 'portfolio',
    loadChildren: () => import('./shared/components/our-portfolio/our-portfolio.module').then(m => m.OurPortfolioModule)
  },
  {
    path: 'pricing',
    loadChildren: () => import('./shared/components/our-prices/our-prices.module').then(m => m.OurPricesModule)
  },
  {
    path: 'services/:id',
    loadChildren: ()=> import('./shared/components/services-id/services-id.module').then(m=> m.ServicesIdModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/error-page/error-page.module').then(m=> m.ErrorPageModule)
  }

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
